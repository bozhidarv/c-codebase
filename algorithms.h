#pragma once
#include <stdio.h>

void bubbleSort(int *arr, size_t length);
void selectionSort(int *arr, size_t length);
