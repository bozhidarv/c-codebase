CC = clang
CFLAGS = --debug

compile-win:
	$(CC) $(CFLAGS) main.c dataStructures.c algorithms.c -o .\out\main.exe

compile:
	$(CC) $(CFLAGS) main.c dataStructures.c algorithms.c -o ./out/main

clean:
	rm -r ./out/*
