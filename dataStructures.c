#include "dataStructures.h"
#include <stdlib.h>

void pushFirstLL(struct LinkedList *linkedList, int value) {
  struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
  (*newNode).value = value;
  (*newNode).next = NULL;

  if (linkedList->size != 0) {
    (*newNode).next = (*linkedList).head;
  }
  (*linkedList).head = newNode;

  linkedList->size++;
}

void pushLastLL(struct LinkedList *linkedList, int value) {
  struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
  (*newNode).value = value;
  (*newNode).next = NULL;

  if (linkedList->size == 0) {
    linkedList->head = newNode;
  } else {

    struct Node *currNode = linkedList->head;
    while (currNode->next != 0) {
      currNode = currNode->next;
    }
    currNode->next = newNode;
  }
  linkedList->size++;
}

void pushAtLL(struct LinkedList *linkedList, int value, size_t id) {
  struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
  (*newNode).value = value;
  (*newNode).next = NULL;

  if (id > linkedList->size || id < 0) {
    return;
  }

  if (linkedList->size == 0 || id == 0) {
    pushFirstLL(linkedList, value);
    return;
  }

  if (id == linkedList->size) {
    pushLastLL(linkedList, value);
    return;
  }

  struct Node *currNode = linkedList->head;
  for (size_t i = 0; i < id - 1; i++) {
    currNode = currNode->next;
  }

  newNode->next = currNode->next;
  currNode->next = newNode;
  linkedList->size++;
}

void deleteFirstLL(struct LinkedList *linkedList) {
  if (linkedList->size == 0) {
    return;
  }

  struct Node *newHead = linkedList->head->next;
  free(linkedList->head);

  linkedList->head = newHead;
  linkedList->size--;
}

void deleteLastLL(struct LinkedList *linkedList) {
  if (linkedList->size == 0) {
    return;
  }

  struct Node *currNode = linkedList->head;
  while (currNode->next != 0) {
    currNode = currNode->next;
  }

  free(currNode);

  linkedList->size--;
}

void deleteAtLL(struct LinkedList *linkedList, size_t id) {
  if (linkedList->size == 0) {
    return;
  }

  if (id > linkedList->size || id < 0) {
    return;
  }

  if (linkedList->size == 0 || id == 0) {
    deleteFirstLL(linkedList);
    return;
  }

  if (id == linkedList->size) {
    deleteLastLL(linkedList);
    return;
  }

  struct Node *currNode = linkedList->head;
  for (size_t i = 0; i < id - 1; i++) {
    currNode = currNode->next;
  }

  struct Node *newNext = currNode->next->next;

  free(currNode->next);
  currNode->next = newNext;

  linkedList->size--;
}

struct Node *getAtLL(struct LinkedList *linkedList, size_t id) {
  if (id < 0 || id >= linkedList->size) {
    return NULL;
  }

  struct Node *currNode = linkedList->head;
  for (size_t i = 0; i < id; i++) {
    currNode = currNode->next;
  }

  return currNode;
}
