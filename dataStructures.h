#pragma once
#include <stdio.h>

struct Node {
  struct Node *next;
  int value;
};

struct LinkedList {
  struct Node *head;
  size_t size;
};

void pushFirstLL(struct LinkedList *linkedList, int value);
void pushLastLL(struct LinkedList *linkedList, int value);
void pushAtLL(struct LinkedList *linkedList, int value, size_t id);
void deleteFirstLL(struct LinkedList *linkedList);
void deleteLastLL(struct LinkedList *linkedList);
void deleteAtLL(struct LinkedList *linkedList, size_t id);
struct Node *getAtLL(struct LinkedList *linkedList, size_t id);
