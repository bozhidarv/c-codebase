#include "algorithms.h"

void bubbleSort(int *arr, size_t length) {
  int isSorted = 1;
  for (size_t i = 0; i < length; i++) {
    isSorted = 1;
    for (size_t j = 0; j < length - 1 - i; j++) {
      if (*(arr + j) > *(arr + j + 1)) {
        int temp = *(arr + j);
        *(arr + j) = *(arr + j + 1);
        *(arr + j + 1) = temp;
        isSorted = 0;
      }
    }
    if (isSorted) {
      break;
    }
  }
}

void selectionSort(int *arr, size_t length) {
  for (size_t i = 0; i < length; i++) {
    size_t minIndex = i;
    for (size_t j = i + 1; j < length; j++) {
      if (*(arr + j) < *(arr + minIndex)) {
        minIndex = j;
      }
    }
    int temp = *(arr + i);
    *(arr + i) = *(arr + minIndex);
    *(arr + minIndex) = temp;
  }
}
