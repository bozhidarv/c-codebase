#include "algorithms.h"
#include "dataStructures.h"
#include <stdlib.h>

int main(void) {
  struct LinkedList linkedList = {NULL, 0};

  size_t size;
  scanf("%zu", &size);

  int *arr = (int *)malloc(size);

  int num;
  for (size_t i = 0; i < size; i++) {
    scanf("%d", &num);
    *(arr + i) = num;
  }

  selectionSort(arr, size);

  for (size_t i = 0; i < size; i++) {
    printf("%i ", *(arr + i));
  }

  printf("\n");

  return 0;
}
